package net.njcp.icodes.constant;

/**
 * @ClassName EndTypeConst
 * @Description: TODO
 * @Author 柳拓
 * @Date 2019/8/19
 * @Version V1.0
 **/
public enum EndTypeConst {

    //1、表示小端，0、表示大端口
    BIG(0, "大端"),

    //1、表示小端，0、表示大端口
    SMALL(1, "小端");

    private int value;

    private String name;

    EndTypeConst(int value,String name){
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        // TODO Auto-generated method stub
        return value;
    }

    public String getName(){
        return name;
    }

    public static EndTypeConst getEndTypeByValue(int value){
        for(EndTypeConst endTypeConst : EndTypeConst.values()){
            if(value == endTypeConst.value){
                return endTypeConst;
            }
        }
        return null;
    }

}
