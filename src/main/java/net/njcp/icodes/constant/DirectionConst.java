package net.njcp.icodes.constant;

/**
 * @ClassName DirectionConst
 * @Description: TODO
 * @Author 柳拓
 * @Date 2019/8/19
 * @Version V1.0
 **/
public enum  DirectionConst {

    //0：终端发出(rtue)、
    MASTER(0, "终端发出"),

    //1：电表发出应答
    SLAVE(1, "电表发出");

    private int value;

    private String name;

    DirectionConst(int value,String name){
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        // TODO Auto-generated method stub
        return value;
    }

    public String getName(){
        return name;
    }

    public static DirectionConst getDirectionByValue(int value){
        for(DirectionConst directionConst : DirectionConst.values()){
            if(value == directionConst.value){
                return directionConst;
            }
        }
        return null;
    }

}
