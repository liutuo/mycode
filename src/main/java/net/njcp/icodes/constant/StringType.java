package net.njcp.icodes.constant;

/**
 * @ClassName StringType
 * @Description: TODO
 * @Author 柳拓
 * @Date 2019/8/24
 * @Version V1.0
 **/
public enum  StringType {
    IEEE754("ieee754","浮点型数据字符的转化"),

    BINARY("binary", "二进制字符串"),

    DEC("dec","十进制double字符串"),

    DHEX("dhex","十六进制double字符串"),

    HEX("hex", "十六进值字符串"),


    ASC("asc","十六进制ASCII字符串"),

    GBK("gbk", "GBK字符串"),

    UTF8("utf-8", "UTF8字符串");

    private String value;

    private String name;

    StringType(String value,String name){
        this.value = value;
        this.name = name;
    }

    public String getValue() {
        // TODO Auto-generated method stub
        return value;
    }

    public String getName(){
        return name;
    }

    public static StringType getStringTypeByValue(String value){
        if(value==null)
            return null;
        for(StringType stringType : StringType.values()){
            if(value.equals(stringType.value)){
                return stringType;
            }
        }
        return null;
    }

}
