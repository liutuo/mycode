package net.njcp.icodes.codec;

import lombok.Data;

/**
 * @Author: 李志鹏
 * @Date: 2020/7/4 16:30
 * @Desc:
 * @param:
 * @return:
 **/

@Data
public class InitListType {
    public static final int FIXED_SIZE = 0;
    public static final int FIXED_LENGTH = 1;
    public static final int FIXED_END = 2;

    private int type;
    private int num;

    public InitListType(int type,int num){
        this.type=type;
        this.num=num;
    }
}
