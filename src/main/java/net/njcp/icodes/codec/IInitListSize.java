package net.njcp.icodes.codec;

/**
 * @ClassName IInitListSize
 * @Description: TODO
 * @Author 柳拓
 * @Date 2019/8/21
 * @Version V1.0
 **/
public interface IInitListSize {

    public InitListType initSize(Object object);

}
