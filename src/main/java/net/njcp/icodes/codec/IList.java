package net.njcp.icodes.codec;

/**
 * @Author: 李志鹏
 * @Date: 2019/12/19 21:15
 * @Desc:
 **/
public interface IList {
    public int getSize();
}
