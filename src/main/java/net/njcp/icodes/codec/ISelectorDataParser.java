package net.njcp.icodes.codec;

import net.njcp.icodes.bo.Configuration;

import java.nio.ByteBuffer;

/**
 * @ClassName ISwitchDataParser
 * @Description: TODO
 * @Author 柳拓
 * @Date 2019/8/19
 * @Version V1.0
 **/
public interface ISelectorDataParser {

    public String selectNamespace(Object upper, Object object, Configuration configuration,final ByteBuffer byteBuffer);

}
