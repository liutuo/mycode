package net.njcp.icodes.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * @ClassName ClassConvertUtil
 * @Description: TODO
 * @Author 柳拓
 * @Date 2019/8/20
 * @Version V1.0
 **/
public class ClassConvertUtil {

    //首字母转小写
    public static String toLowerCaseFirstOne(String s){
        if(Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }
    //首字母转大写
    public static String toUpperCaseFirstOne(String s){
        if(Character.isUpperCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
    }

    //获取字段的范型类
    public static Class<?> getParadigm(Field nameField){
        Class<?> aClass = null;
        Type genericType = nameField.getGenericType();
        if(genericType instanceof ParameterizedType){
            ParameterizedType pt = (ParameterizedType) genericType;
            //得到泛型里的class类型对象
            aClass = (Class<?>)pt.getActualTypeArguments()[0];
        }
        return aClass;
    }

}
