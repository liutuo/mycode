package net.njcp.icodes.bo;

import lombok.Data;

/**
 * @ClassName ParserModul
 * @Description: TODO
 * @Author 柳拓
 * @Date 2019/8/16
 * @Version V1.0
 **/
@Data
public class ParserModul {

    //attribute的ID
    private String id;

    //attribute的数据类型
    private String type;

    //开始位
    private Short startBit;

    //多少位
    private Short sumBit;

    //端类型1、表示小端，0、表示大端口
    private Short endType;

    //偏移量
    private Integer offset;

    //精度
    private Double accuracy;

    //字符串类型：2进制、16进制、GBK、UTF-8
    private String stringType;

    //解析方法
    private String iParserMethod;

    //自定义选择接口实现
    private String iSelectorDataParser;

    //初始化数据单元的个数，针对List类型
    private String iInitListSize;


}
