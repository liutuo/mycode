package net.njcp.icodes.bo;

import lombok.Data;
import net.njcp.icodes.codec.IInitListSize;
import net.njcp.icodes.codec.IList;
import net.njcp.icodes.codec.ISelectorDataParser;
import net.njcp.icodes.codec.InitListType;
import net.njcp.icodes.constant.EndTypeConst;
import net.njcp.icodes.constant.StringType;
import net.njcp.icodes.util.*;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Parser
 * @Description: TODO
 * @Author 柳拓
 * @Date 2019/8/19
 * @Version V1.0
 **/
@Data
public class Parser {

    private Configuration parserConfig;

    private List<ParserModul> parserModuls;

    private Class<?> cls;

    public Parser(){

    }

    public Parser(Configuration parserConfig, List<ParserModul> parserModuls, String namespace){
        this.parserConfig = parserConfig;
        this.parserModuls = parserModuls;
        this.cls = getClass(namespace);
    }

    //根据类路径拿到对象
    public Class getClass(String namespace){
        //
        String name = namespace;
        if(parserConfig.getNameByAliase(namespace) != null){
            name = parserConfig.getNameByAliase(namespace);
        }
        Class<?> cls = null;
        try {
            cls = Class.forName(name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return cls;
    }

    //获取字段
    public Field getField(String modulId){
        Field nameField = null; // 获得name属性
        try {
            nameField = cls.getDeclaredField(modulId);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        nameField.setAccessible(true); //解除封装
        return nameField;
    }

    //获取字段类型对象
    public Class<?> getFieldType(String modulType){
        return (Class<?>) getClass(modulType);
    }

    //获取指定长度的字节
    public byte[] getBytes(ParserModul parserModul,ByteBuffer byteBuffer){
        byte[] bytes = null;
        short startBit = parserModul.getStartBit();
        short sumBit = parserModul.getSumBit();
//        if (byteBuffer.position() == byteBuffer.limit()){
//            return new byte[]{0x00};
//        }
        //如果开始位为0，位数位8的整数，那么取完整字节
        if(startBit == 0 && sumBit%8==0){
            bytes = new byte[sumBit/8];
            byteBuffer.get(bytes);
        //如果开始地址加位数小于8那么不是一个完整字节
        }else if(startBit + sumBit < 8){
            bytes = new byte[1];
            //这种情况下，我们将不移动下标
            int position = byteBuffer.position();
            byte b = byteBuffer.get();
            byteBuffer.position(position);
            b = ConvertUtil.getBits(b, startBit, sumBit);
            bytes[0] = b;
        //如果开始地址加位数大于8那么超个一个字节但不完整
        }else if(startBit + sumBit >= 8 && sumBit%8!=0){
            //先获取字节数
            int byteNum = sumBit/8+1;
            bytes = new byte[byteNum];
            //一、从0开始
            if (startBit == 0){
                if(byteNum > 1){
                    //先获取完整部分的字节数组
                    byte[] numbytes = new byte[byteNum-1];
                    byteBuffer.get(numbytes);
                    for(int i=0; i<numbytes.length; i++){
                        bytes[i] = numbytes[i];
                    }
                }
                //再获取不足一个字节部分的数据
                int position = byteBuffer.position();
                byte b = byteBuffer.get();
                byteBuffer.position(position);
                b = ConvertUtil.getBits(b, 0, sumBit%8);
                bytes[byteNum-1] = b;
            }else{
                //如果不是从0开始，我们先获取不完整的部分
                //适用于长度大于1字节，并且颠倒
                byte b = byteBuffer.get();
                b = ConvertUtil.getBits(b, startBit, sumBit%8);
                bytes[0] = b;
                if(byteNum > 1){
                    byte b2 =byteBuffer.get(byteBuffer.position());
                    b2 = (byte)(((b2<<(8-startBit))&0xff)+b);
                    bytes[0] = b2;
                    //获取完整部分的字节数组
                    byte[] numbytes = new byte[byteNum-1];
                    byteBuffer.get(numbytes);
                    for (int i = 0;i<numbytes.length;i++){
                        numbytes[i]=(byte)(numbytes[i]>>startBit);
                    }
                    System.arraycopy(numbytes, 0, bytes, 1, numbytes.length);
                }
            }
        }
        return bytes;
    }

    public void writeBuffer(ParserModul parserModul, ByteBuffer byteBuffer, byte[] bytes){
        short startBit = parserModul.getStartBit();
        short sumBit = parserModul.getSumBit();
        //如果开始位为0，位数位8的整数，那么取完整字节
        if(startBit == 0 && sumBit%8==0){
            byteBuffer.put(bytes);
        //如果开始地址加位数小于8那么不是一个完整字节
        }else if(startBit + sumBit < 8){
            int position = byteBuffer.position();
            if(startBit == 0){
                //如果为起始位，直接新建一位
                byte value =  ConvertUtil.setBits(bytes[0], (byte) 0x00, startBit);
                byteBuffer.put(value);
            }else if(startBit != 0){
                byte[] array = byteBuffer.array();
                //如果不等于0，那么取道当前的值，将值进行位运算
                byte value = array[position];
                byte newValue =  ConvertUtil.setBits(bytes[0], value, startBit);
                byteBuffer.put(newValue);
            }
            //将下标设置回去
            byteBuffer.position(position);
        //如果开始地址加位数大于8那么超个一个字节但不完整
        }else if(startBit + sumBit >= 8 && sumBit%8!=0){
            int byteNum = sumBit/8+1;
            //如果从0开始，先将完整字节的部分写入缓冲区
            if (startBit == 0) {
                //先将完整字节写入
                if (byteNum > 1){
                    for (int i=0; i<byteNum-1; i++){
                        byteBuffer.put(bytes[i]);
                    }
                }
                //获取最后不足一个字节的部分
                byte value =  ConvertUtil.setBits(bytes[byteNum-1], (byte) 0x00, startBit);
                int position = byteBuffer.position();
                byteBuffer.put(value);
                byteBuffer.position(position);
            //如果是前面多了一部分
            }else {
                //先将前面不足一个字节的部分取出
                byte[] array = byteBuffer.array();
                //获取当前位置
                int position = byteBuffer.position();
                byte value = array[position];
                flipBytes(parserModul,bytes);
                byte[] newValue =  ConvertUtil.setBits(bytes, value, startBit);
                flipBytes(parserModul,newValue);
                byteBuffer.put(newValue);
//                //再将完整字节写入
//                if (byteNum > 1){
//                    for (int i=1; i<byteNum; i++){
//                        byteBuffer.put(bytes[i]);
//                    }
//                }

            }
        }
    }

    //乘以精度,获取小数
    public static String getByAccuracy(Long longValue, Double accuracy){
        BigDecimal bigDecimal = new BigDecimal(longValue.toString());
        BigDecimal bigDecimal1 = new BigDecimal(accuracy.toString());
        return bigDecimal.multiply(bigDecimal1).toString();
    }

    //除以进度，获取整数
    public static long getByAccuracy(String doubleValue, Double accuracy){
        BigDecimal bigDecimal = new BigDecimal(doubleValue);
        BigDecimal bigDecimal1 = new BigDecimal(accuracy.toString());
        return bigDecimal.divide(bigDecimal1,0,BigDecimal.ROUND_DOWN).longValue();
    }


    public static void main(String[] args) {
        long a =  getByAccuracy("123.456789",0.001);
        System.out.println(a);
    }

    //根据字段类型，将字节转成具体的类型
    public String getBytesValue(Class<?> fieldType, ParserModul parserModul, byte[] bytes){
        String value = "";
        if(fieldType.equals(Byte.class)) {
            if(parserModul.getStringType()!=null&&parserModul.getStringType().equals("dec"))
                value = Integer.toHexString(bytes[0]&0xff);
            else
                value = String.valueOf(bytes[0]);
        }else if(fieldType.equals(Short.class)){
            value = String.valueOf(ConvertUtil.bytes2short(bytes));
        }else if(fieldType.equals(Integer.class)){
            value = String.valueOf(ConvertUtil.bytes2int(bytes));
        }else if(fieldType.equals(Long.class)){
            value = String.valueOf(ConvertUtil.bytes2long(bytes));
        }else if(fieldType.equals(Float.class)){
            Long longValue = ConvertUtil.bytes2long(bytes);
            value = getByAccuracy(longValue,parserModul.getAccuracy());
        }else if(fieldType.equals(Double.class)){
            Long longValue = ConvertUtil.bytes2long(bytes);
            value = getByAccuracy(longValue,parserModul.getAccuracy());
        }else if(fieldType.equals(Character.class)){

        }else if(fieldType.equals(String.class)){
            StringType stringType = StringType.getStringTypeByValue(parserModul.getStringType());
            switch (stringType){
                case IEEE754:
                    value = ConvertUtil.bytes2hexstr(bytes);
                    double valued =IEEE754.toValue(value);
                    if(parserModul.getAccuracy() != null && !parserModul.getAccuracy().equals(1.0)){
                            BigDecimal bigDecimal = new BigDecimal(Double.toString(valued));
                            BigDecimal bigDecimal1 = new BigDecimal(parserModul.getAccuracy());
                            valued = bigDecimal.multiply(bigDecimal1).doubleValue();
                        }
                    value = String.valueOf(valued);
                    break;
                case BINARY:
                    value = ConvertUtil.byteArrToBinStr(bytes);
                    break;
                case DEC:
                    value = ConvertUtil.bytes2dec(bytes);
                    //如果是带精度的数据，要乘以精度再转
                    try{
                        if(parserModul.getAccuracy() != null && !parserModul.getAccuracy().equals(1.0)){
                            value = getByAccuracy(Long.parseLong(value), parserModul.getAccuracy());
                        }
                    }catch (Exception e){
//                        System.out.println(value+"不是十进制数！");
                    }
                    break;
                case DHEX:
                    value = ConvertUtil.bytes2hexstr(bytes);
                    //如果是带精度的数据，要乘以精度再转
                    if(parserModul.getAccuracy() != null && !parserModul.getAccuracy().equals(1.0)){
                        value = getByAccuracy(Long.parseLong(value,16), parserModul.getAccuracy());
                    }
                    break;
                case HEX:
                    value = ConvertUtil.bytes2hexstr(bytes);
                    break;
                case ASC:
                    value = new String(bytes);
                    break;
                case GBK:
                    try {
                        value = new String(bytes, "GBK");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    break;
                case UTF8:
                    value = new String(bytes, StandardCharsets.UTF_8);
                    break;
                default:
                    break;
            }
        }
        return value;
    }

    //根据字段类型，将字节转成具体的字节
    public byte[] getValueBytes(Class<?> fieldType, ParserModul parserModul, Object fieldObject){
        String value =fieldObject==null?"0":fieldObject.toString();
        byte[] valueBytes = null;
        byte[] returnBytes = null;
        if(parserModul.getSumBit()%8 == 0){
            returnBytes = new byte[parserModul.getSumBit()/8];
        }else{
            returnBytes = new byte[parserModul.getSumBit()/8+1];
        }
        if(fieldType.equals(Byte.class)) {
            StringType stringType = StringType.getStringTypeByValue(parserModul.getStringType());
            valueBytes = new byte[1];
            if(stringType!=null&&stringType.equals(StringType.DEC))
                valueBytes[0] = Byte.valueOf(value,16);
            else
                valueBytes[0] = Byte.parseByte(value);
        }else if(fieldType.equals(Short.class)){
            valueBytes = ConvertUtil.short2bytes(Short.parseShort(value));
        }else if(fieldType.equals(Integer.class)){
            valueBytes = ConvertUtil.int2bytes(Integer.parseInt(value));
        }else if(fieldType.equals(Long.class)){
            valueBytes = ConvertUtil.long2bytes(Long.parseLong(value));
        }else if(fieldType.equals(Float.class)){
            //先将浮点型数据乘以精度转成long类型
            long value1 = getByAccuracy(value,parserModul.getAccuracy());
            valueBytes = ConvertUtil.long2bytes(value1);
        }else if(fieldType.equals(Double.class)){
            //先将浮点型数据乘以精度转成long类型
            long value1 = getByAccuracy(value,parserModul.getAccuracy());
            valueBytes = ConvertUtil.long2bytes(value1);
        }else if(fieldType.equals(Character.class)){

        }else if(fieldType.equals(String.class)){
            StringType stringType = StringType.getStringTypeByValue(parserModul.getStringType());
            switch (stringType){
                case IEEE754:
                    String hex = IEEE754.toHex(Double.valueOf(value), true);
                    valueBytes = ConvertUtil.hexstr2bytes(hex);
                    break;
                case BINARY:
                    valueBytes = ConvertUtil.binStrToByteArr(value);
                    break;
                case ASC:
                    value = ConvertUtil.bytes2hexstr(value.getBytes());
                    valueBytes = ConvertUtil.hexstr2bytes(value);
                    break;
                case DEC:
                    //如果是带精度的数据，先将精度除去，再转成字节
                    if(parserModul.getAccuracy() != null && !parserModul.getAccuracy().equals(1.0)){
                        try {
                            long value1 = getByAccuracy(value,parserModul.getAccuracy());
                            valueBytes = ConvertUtil.hexstr2bytes(Long.toString(value1));
                        }catch (Exception e){
                            valueBytes = ConvertUtil.hexstr2bytes(value);
//                            System.out.println(value+"不是十进制数！");
                        }
                    }else{
                        valueBytes = ConvertUtil.hexstr2bytes(value);
                    }
                    break;
                case HEX:
                    //如果是带精度的数据，先将精度除去，再转成字节
                    if(parserModul.getAccuracy() != null && !parserModul.getAccuracy().equals(1.0)){
                        long value1 = getByAccuracy(value,parserModul.getAccuracy());
                        valueBytes = ConvertUtil.hexstr2bytes(Long.toString(value1));
                    }else{
                        valueBytes = ConvertUtil.hexstr2bytes(value);
                    }
                    break;
                case GBK:
                    try {
                        valueBytes = value.getBytes("GBK");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    break;
                case UTF8:
                    valueBytes = value.getBytes(StandardCharsets.UTF_8);
                    break;
                default:
                    break;
            }

        }
        for (int i=0; i<returnBytes.length; i++) {
            if (valueBytes.length-i-1>=0){
                returnBytes[returnBytes.length-i-1] = valueBytes[valueBytes.length-i-1];
            }else {
                returnBytes[returnBytes.length-i-1] = 0;
            }

        }
        return returnBytes;
    }

    //根据解析的结果，构造一个字段一样数据类型的对象
    public Object getFieldResult(Class<?> fieldType, String value){
        Object fieldObject = null;
        try {
            Constructor<?> cons = null;
            try {
                cons = fieldType.getConstructor(String.class);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            try {
                assert cons != null;
                fieldObject = cons.newInstance(value);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldObject;
    }

    //给字段设置数据
    public Object valueOf(Field nameField, Object object, Object value){
        //如果为enum类型，则通过getValue来实例化
        if(cls.isEnum()){
            Object[] obs = cls.getEnumConstants();
            Method getValue =null;
            try {
                 getValue = cls.getMethod("getValue");
                for (Object o : obs) {
                    String v =getValue.invoke(o).toString();
                    if(v.equals(value.toString())){
                        nameField.set(object, o);
                        return o;
                    }
                }
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }

        }
        else {
            try {
                if(!nameField.getType().equals(List.class)||value instanceof List) {
                    nameField.set(object, value);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return object;
    }

    //判断数据是大端还是小端，大端不翻转，小端翻转
    public void flipBytes(ParserModul parserModul, byte[] bytes){
        if (parserModul.getEndType() == EndTypeConst.SMALL.getValue()){
            byte[] newbytes = bytes.clone();
            for (int i=0; i<newbytes.length; i++){
                bytes[newbytes.length-i-1] = newbytes[i];
            }
        }
    }

    //加上字节偏移量
    public void addOffsetBytes(ParserModul parserModul, byte[] bytes){
        if (parserModul.getOffset() != 0){
            for (int i=0; i<bytes.length; i++){
                bytes[i] = (byte) (bytes[i] + parserModul.getOffset());
            }
        }
    }

    //减去上字节偏移量
    public void subtractOffsetBytes(ParserModul parserModul, byte[] bytes){
        if (parserModul.getOffset() != 0){
            for (int i=0; i<bytes.length; i++){
                bytes[i] = (byte) (bytes[i] - parserModul.getOffset());
            }
        }
    }

    //获取类路径
    public String getClassName(Class<?> fieldType, ParserModul parserModul, Object obj, Object upperObject,final ByteBuffer byteBuffer){
        String nameSpace = "";
        //如果是接口类型通过配置的选择器，找到实现类的路径
        if(fieldType.isInterface()){
            String iswitch = parserModul.getISelectorDataParser();
            try {
                ISelectorDataParser switchDataParser = (ISelectorDataParser) getClass(iswitch).newInstance();
                nameSpace= switchDataParser.selectNamespace(upperObject,obj, parserConfig,byteBuffer);
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }else if(parserModul.getType().equals("java.util.List")){
            nameSpace = ClassConvertUtil.toLowerCaseFirstOne(fieldType.getSimpleName());
        }else{
            //如果字段为对象类型
            nameSpace = parserModul.getType();
        }
        return nameSpace;
    }

    public InitListType getListSize(ParserModul parserModul, Object obj){
        String initSize = parserModul.getIInitListSize();
        InitListType type = null;
        try {
            IInitListSize iInitListSize = (IInitListSize) getClass(initSize).newInstance();
            type = iInitListSize.initSize(obj);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return type;
    }

    public Object decode(ByteBuffer byteBuffer){
        return decode(byteBuffer,null, null);
    }

    public Object decode(ByteBuffer byteBuffer,Field upperField, Object upperObject){
        Object obj = PackageUtil.getObject(cls);
        if (upperObject != null) {
            //将对象类型字段赋值给obj
            valueOf(upperField, upperObject, obj);
        }
        for (ParserModul parserModul : parserModuls) {
            Field nameField = getField(parserModul.getId());
            Class<?> fieldType = getFieldType(parserModul.getType());
            //判断该参数是否为包装类型
            if (parserModul.getType().contains("java.lang")) {
                //获取每部分的字节数组
                byte[] bytes = getBytes(parserModul, byteBuffer);
                //判断大端小端是否需要颠倒字节序
                flipBytes(parserModul, bytes);
                //处理字节偏移量
                addOffsetBytes(parserModul, bytes);
                //将最后处理的字节转成数据
                String value = getBytesValue(fieldType, parserModul, bytes);
                //实例化一个字段对象
                Object filedResult = getFieldResult(fieldType, value);
                //将该实例赋值给字段
                boolean isEnum = (obj instanceof Class)&&(((Class)obj).isEnum());
                obj = valueOf(isEnum?upperField:nameField, isEnum?upperObject:obj, filedResult);
            } else if (fieldType.equals(List.class)) {
                List list = new ArrayList<>();
                InitListType type =obj instanceof IList ? getListSize(parserModul, obj):getListSize(parserModul, upperObject);
                if(type.getType()==InitListType.FIXED_SIZE) {
                    for (int i = 0; i < type.getNum(); i++) {
                        //获取list的范型
                        Class<?> fieldParadigm = ClassConvertUtil.getParadigm(nameField);
                        //获取对应解析类的地址
                        String nameSpace = getClassName(fieldParadigm, parserModul, obj, upperObject, byteBuffer);
                        if (nameSpace != null && !nameSpace.equals("")) {
                            Parser parser = parserConfig.getParser(nameSpace);
                            //解析对象类型,并将上一层对象设置到解析方法，方便在下层获取数据
                            Object object = parser.decode(byteBuffer, nameField, obj);
                            //将对象类型字段赋值给obj
                            list.add(object);
                        }
                    }
                }else if(type.getType()==InitListType.FIXED_END){
                    while (byteBuffer.limit()-byteBuffer.position()>type.getNum()) {
                        //获取list的范型
                        Class<?> fieldParadigm = ClassConvertUtil.getParadigm(nameField);
                        //获取对应解析类的地址
                        String nameSpace = getClassName(fieldParadigm, parserModul, obj, upperObject, byteBuffer);
                        if (nameSpace != null && !nameSpace.equals("")) {
                            Parser parser = parserConfig.getParser(nameSpace);
                            //解析对象类型,并将上一层对象设置到解析方法，方便在下层获取数据
                            Object object = parser.decode(byteBuffer, nameField, upperObject);
                            //将对象类型字段赋值给obj
                            list.add(object);
                        }
                    }
                }
                valueOf(nameField, obj, list);
            } else {
                //如果不是包装类型，我们先要获取类路径
                String nameSpace = getClassName(fieldType, parserModul, obj,upperObject, byteBuffer);
                if (nameSpace != null && !nameSpace.equals("")) {
                    Parser parser = parserConfig.getParser(nameSpace);
                    //解析对象类型,并将上一层对象设置到解析方法，方便在下层获取数据
                    parser.decode(byteBuffer, nameField, obj);
                }
            }
        }
        return obj;
    }

    public void encode(ByteBuffer byteBuffer, Object obj){
        encode(byteBuffer, obj, null);
    }

    public void encode(ByteBuffer byteBuffer, Object obj, Object upperObject){
        upperObject = upperObject==null?obj:upperObject;
        byte[] fieldBytes = null;
        for (ParserModul parserModul:parserModuls) {
            Field nameField = getField(parserModul.getId());
            Class<?> fieldType = getFieldType(parserModul.getType());
            Object fieldObject = null;
            //判断该参数是否为包装类型
            if(parserModul.getType().contains("java.lang")){
                try {
                    //获取字段的数据
                    fieldObject = nameField.get(obj);
                    //将数据转成字节数组
                    fieldBytes = getValueBytes(fieldType, parserModul, fieldObject);
                    //处理字节偏移量
                    subtractOffsetBytes(parserModul, fieldBytes);
                    //判断大端小端是否需要颠倒字节序
                    flipBytes(parserModul,fieldBytes);
                    //将字节写入字节缓冲区
                    writeBuffer(parserModul, byteBuffer, fieldBytes);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }else if(fieldType.equals(List.class)) {
                //获取list的范型
                Class<?> fieldParadigm = ClassConvertUtil.getParadigm(nameField);
                //获取对应解析类的地址
                String nameSpace = getClassName(fieldParadigm, parserModul, obj, upperObject,byteBuffer);
                if(nameSpace != null && !nameSpace.equals("")){
                    try {
                        //获取字段对象
                        fieldObject = nameField.get(obj);
                        List<?> objects = (List<?>) fieldObject;
                        for (int i=0; i< objects.size(); i++){
                            Parser parser = parserConfig.getParser(nameSpace);
                            parser.encode(byteBuffer, objects.get(i),upperObject);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }else {
                //如果不是包装类型，我们先要获取类路径
                String nameSpace = getClassName(fieldType, parserModul, obj, upperObject,byteBuffer);
                if (nameSpace != null && !nameSpace.equals("")){
                    Parser parser = parserConfig.getParser(nameSpace);
                    //解析对象类型
                    try {
                        fieldObject = nameField.get(obj);
                        //处理在解析过程中生成的对象，例如校验位
                        if (fieldObject == null){
                            try {
                                Constructor<?> cons;
                                try {
                                    cons = fieldType.getConstructor(ByteBuffer.class);
                                    fieldObject = cons.newInstance(byteBuffer);
                                } catch (NoSuchMethodException | InvocationTargetException e) {
                                    e.printStackTrace();
                                }
                            } catch (InstantiationException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    parser.encode(byteBuffer, fieldObject,upperObject);
                }
            }
        }
    }

}
