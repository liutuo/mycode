package net.njcp.icodes.bo;

/**
 * @Author: 李志鹏
 * @Date: 2020/3/19 17:06
 * @Desc:
 **/
public interface ParserTool {
    <T> T decode(byte[] bytes) throws Exception;
}
