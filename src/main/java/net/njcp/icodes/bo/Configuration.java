package net.njcp.icodes.bo;

import net.njcp.icodes.builder.XMLConfigBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName Configuration
 * @Description: TODO
 * @Author 柳拓
 * @Date 2019/8/16
 * @Version V1.0
 **/
public class Configuration {

    private static Map<String, List<ParserModul>> parser = new HashMap<>();

    private static Map<String, String> aliases = new HashMap<>();

    private static Map<String, Map<String, String>> selectorses = new HashMap<>();

    private XMLConfigBuilder xmlConfigBuilder;

    public void registerParser(String namespace, List<ParserModul> parserModuls){
        parser.put(namespace, parserModuls);
    }

    public void registerAliases(String alias, String namespace){
        aliases.put(alias, namespace);
    }

    public void registerSelectors(String namespace, Map<String, String> selectors){
        selectorses.put(namespace, selectors);
    }

    public Parser getParser(String namespace){
        List<ParserModul> parserModuls = parser.get(namespace);
        return new Parser(this, parserModuls, namespace);
    }

    public String getNameByAliase(String aliase){
        return aliases.get(aliase);
    }

    public Map<String, String> getSelectors(String selectorNmae){
        return selectorses.get(selectorNmae);
    }

}
